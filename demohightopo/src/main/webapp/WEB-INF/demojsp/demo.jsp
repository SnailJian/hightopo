<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<script src="../../script/hightopo/lib/core/ht.js"></script>
<script src="../../script/hightopo/lib/plugin/ht-cssanimation.js"></script> <!--依赖cssanimation插件-->
<script src="../../script/hightopo/lib/plugin/ht-palette.js"></script>
<title>high topo demo</title>
</head>
<body>
	<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; overflow: hidden; top: 0px; right: 0px; bottom: 0px; left: 0px;">
		<div tabindex="-1" class="ht-widget-palette" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; background: rgb(255, 255, 255); overflow: hidden; user-select: none; left: 0px; top: 0px; width: 200px; height: 833px;">
			<div class="palette-header" style="position: relative; background: rgb(44, 62, 80); color: rgb(255, 255, 255); top: 0px; box-sizing: border-box; padding: 0px 5px 0px 0px; border-top: 1px solid rgb(129, 140, 150); width: 100%; cursor: pointer; white-space: nowrap; overflow: hidden; font: 12px/24px arial, sans-serif;">
				<canvas width="16" height="24" class="palette-toggle-icon-2" style="cursor: pointer; display: inline-block; margin-right: 4px; vertical-align: top; width: 16px; height: 24px;"></canvas>
				<span style="font: 12px arial, sans-serif;">Nodes</span>
			</div>
			<div class="palette-content" style="max-height: 103px; font: 12px arial, sans-serif; overflow: hidden; transition-duration: 200ms; padding-bottom: 10px; transition-property: max-height, padding-bottom;">
				<div class="palette-item" title="music.png" style="vertical-align: top; cursor: pointer; border-radius: 5px; border: 1px solid transparent; text-align: center; display: inline-block; margin-left: 10px; margin-top: 10px; color: rgb(119, 119, 119);">
					<canvas class="image-box" width="70" height="50" style="width: 70px; height: 50px;"></canvas>
					<div class="label-box" style="max-width: 70px; overflow: hidden;">Node</div>
				</div>
				<div class="palette-item" title="drag me to graphview" style="vertical-align: top; cursor: pointer; border-radius: 5px; border: 1px solid transparent; text-align: center; display: inline-block; margin-left: 10px; margin-top: 10px; color: rgb(119, 119, 119); background: rgb(26, 188, 156);">
					<canvas class="image-box" width="70" height="50" style="width: 70px; height: 50px;"></canvas>
					<div class="label-box" style="max-width: 70px; overflow: hidden;">Draggable Node</div>
				</div>
			</div>
				<div style="cursor: default; position: absolute; left: 0px; top: 0px;"></div>
		</div>
		<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; overflow: hidden; left: 201px; top: 0px; width: 1079px; height: 833px;">
			<canvas width="1079" height="833" style="pointer-events: none; border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; width: 1079px; height: 833px;"></canvas>
			<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box;">
				<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; visibility: hidden;"></div>
				<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; visibility: hidden;"></div>
			</div>
		</div>
		<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; left: 200px; top: 0px; width: 1px; height: 833px; cursor: ew-resize; background: rgb(44, 62, 80);">
			<div tabindex="-1" style="border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; left: -3.5px; top: 0px; width: 8px; height: 833px; cursor: ew-resize;"></div>
			<canvas width="1" height="1" style="pointer-events: auto; border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; cursor: pointer; width: 1px; height: 1px; left: 0px; top: 411.5px;"></canvas>
			<canvas width="1" height="1" style="pointer-events: auto; border: 0px; outline: 0px; padding: 0px; position: absolute; margin: 0px; box-sizing: border-box; cursor: pointer; width: 1px; height: 1px; left: 0px; top: 420.5px;"></canvas>
		</div>
	</div>
</body>
</html>